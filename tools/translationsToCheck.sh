#! bash
#
# translationsToCheck.sh
#
# script to identify the translation to check
# (same text as the English version)
#

test -f ../gcstar/lib/gcstar/GCLang/EN/GCstar.pm || (echo === Execute in the tools directory && exit)

(cd ../gcstar/lib/gcstar/GCLang/;
 for l in *
 do
	 test -d $l && echo $l
 done;
) > Translations_Checks/list_languages.txt

cat Translations_Checks/list_languages.txt | \
	while read i
	do
		(
			cd ../gcstar/lib/gcstar/GCLang/$i
			grep "=>" *.pm */*.pm
		) | sort > Translations_Checks/translations_$i.txt
	done

grep -v EN Translations_Checks/list_languages.txt  | \
	while read i
	do
		echo ============== Checking translations $i ================
		cat Translations_Checks/translations_EN.txt Translations_Checks/translations_$i.txt |\
			sort | uniq -c | grep -v '^ *1 ' > Translations_Checks/translations_to_check_$i.txt
		cat Translations_Checks/translations_to_check_$i.txt
	done
